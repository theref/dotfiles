" File explorer tree in LH sidebar
Plug 'scrooloose/nerdtree'

" Git status flags in nerdtree file explorer
Plug 'albfan/nerdtree-git-plugin'

" Asynchronous Linting Engine
Plug 'dense-analysis/ale'

" Code auto-formatting
Plug 'Chiel92/vim-autoformat'

" Python syntax highlighting
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

" RestructuredText notes and highlighting
Plug 'Rykka/riv.vim'

" Python import statement sorting
Plug 'fisadev/vim-isort'

" Git wrapper (git commands within nvim)
Plug 'tpope/vim-fugitive'

" Comment lines and blocks
Plug 'tpope/vim-commentary'

" Git status flags in LHS gutter
Plug 'airblade/vim-gitgutter'

" Syntax highlighting of .xsh and .xonshrc files
Plug 'linkinpark342/xonsh-vim'

" Syntax for toml files
Plug 'cespare/vim-toml'

" Autocompleter
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

Plug 'davidhalter/jedi-vim'

" Deoplete plugin for Python autocompletion using jedi
Plug 'deoplete-plugins/deoplete-jedi'

" LaTeX integration
Plug 'lervag/vimtex'

" Auto session management
Plug 'tpope/vim-obsession'

" CSV Tools
Plug 'chrisbra/csv.vim'

" Python code folding
Plug 'tmhedberg/SimpylFold'

" Tmux command runner
Plug 'christoomey/vim-tmux-runner'

" This should be after any plugins that it integrates with
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Nord colour theme
Plug 'arcticicestudio/nord-vim'

" Common navigation of tmux and nvim windows
Plug 'christoomey/vim-tmux-navigator'

" This plugin must be the last in the list
" Add icons for use by other plugins
Plug 'ryanoasis/vim-devicons'

