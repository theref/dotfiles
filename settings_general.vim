set nu
set t_Co=256
set colorcolumn=88
let mapleader=","
let maplocalleader=","
let g:python3_host_prog = '/Users/jamescampbell/.pyenv/versions/nvim/bin/python3'
set termguicolors

" Use bash as the shell because xonsh has a bug that stops plugins working
" https://github.com/xonsh/xonsh/issues/3402
set shell=bash

" Airline plugin settings
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='nord'
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline_powerline_fonts = 1

" Nord theme settings
let g:nord_italic = 1
let g:nord_italic_comments = 1
let g:nord_underline = 1
" let g:nord_uniform_status_lines = 1
" let g:nord_uniform_diff_background = 1
let g:nord_cursor_line_number_background = 1
colorscheme nord
set cursorline

" General Settings
set path+=**
:au FocusLost * :wa
set splitbelow
set splitright
autocmd TermOpen * set bufhidden=hide
autocmd TermOpen * setlocal nonumber

" Use deoplete.
let g:deoplete#enable_at_startup = 1

" Custom Key Mappings
set pastetoggle=<Leader>p
noremap <Leader>c :vsplit term://xonsh<CR> :startinsert<CR>
noremap <Leader>ch :split term://xonsh<CR> :startinsert<CR>
noremap <Leader>e :NERDTreeToggle<CR>
noremap <Leader>f :Autoformat<CR>
:autocmd FileType python nnoremap <buffer> <localleader>i :Isort<CR>
inoremap jk <esc>
inoremap <esc> <nop>
nnoremap <silent> <leader>nb :set relativenumber!<CR>
let g:tex_flavor  = 'latex'
let g:tex_conceal = ''
let g:vimtex_fold_manual = 1
autocmd BufWinEnter * silent! :%foldopen!
let g:vimtex_latexmk_continuous = 1
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_method = 'zathura'
let g:VtrUseVtrMaps = 1
let g:VtrStripLeadingWhitespace = 1
let g:VtrClearEmptyLines = 1
